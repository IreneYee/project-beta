"""service_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from service_rest.views import api_technician, api_appointment, api_appointment_detail, api_technician_detail

urlpatterns = [
    path('api/', include("service_rest.urls")),
    path('admin/', admin.site.urls),
    path('technicians/', api_technician, name="api_technician"),
    path('appointments/', api_appointment, name="api_appointment"),
    path('api/appointments/<int:id>/', api_appointment_detail, name='api_appointment_detail'),
    path('api/technicians/<int:id>/', api_technician_detail, name='api_technician_detail'),
]
