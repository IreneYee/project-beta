# CarCar

Team:

* Person 1 - Yutong Ye - Automobile Sales
* Person 2 - Mark Giampa - Automobile Service

## How to Run this App
 Fork the respository and clone it onto your local machine.
    https://gitlab.com/IreneYee/project-beta
        click fork
    From the fork:
        click COPY and select the clipboard for HTTPS
    In your terminal: run : 'git clone [paste the copied https link]'

 Using docker; build, compose, and run containers.
    In your terminal:
        docker volume create beta-data
        docker-compose build
        docker-compose run

 Once all containers are running,
 and react log shows "you can now view app in browser":
    visit 'http://localhost:3000/'


## Diagram
https://excalidraw.com/#json=9eNfJPKUSVjnOW1iC62bz,PJHf5ZszSdVTZOBjeLZy-A


## API Documentation

### URLs and Ports
:INVENTORY URLS:
   | Action | 	                |  Method |           | URL |
List  manufacturers             |   GET	  |  http://localhost:8100/api/manufacturers/
Create a manufacturer           |	POST  |	 http://localhost:8100/api/manufacturers/
Get a specific manufacturer	    |   GET	  |  http://localhost:8100/api/manufacturers/:id/
Update a specific manufacturer	|   PUT	  |  http://localhost:8100/api/manufacturers/:id/
Delete a specific manufacturer	|  DELETE |  http://localhost:8100/api/manufacturers/:id/

:SERVICES URLS:
   | Action  |                    |  Method |           | URL |
List technicians	              |   GET	|  http://localhost:8080/api/technicians/
Create a technician	              |   POST	|  http://localhost:8080/api/technicians/
Delete a specific technician	  |  DELETE	|  http://localhost:8080/api/technicians/:id/
List appointments	              |   GET	|  http://localhost:8080/api/appointments/
Create an appointment	          |   POST	|  http://localhost:8080/api/appointments/
Delete an appointment	          |  DELETE	|  http://localhost:8080/api/appointments/:id/
Set appointment status "canceled" |   PUT   |  http://localhost:8080/api/appointments/:id/cancel/
Set appointment status "finished" |   PUT   |  http://localhost:8080/api/appointments/:id/finish/

:SALES URLS:
   | Action |                 | Method |              |URL |
List salespeople	          |  GET   |  http://localhost:8090/api/salespeople/
Create a salesperson	      |  POST  |  http://localhost:8090/api/salespeople/
Delete a specific salesperson | DELETE |  http://localhost:8090/api/salespeople/:id/
List customers	              |  GET   |  http://localhost:8090/api/customers/
Create a customer	          |  POST  |  http://localhost:8090/api/customers/
Delete a specific customer	  | DELETE |  http://localhost:8090/api/customers/:id/
List sales	                  |  GET   |  http://localhost:8090/api/sales/
Create a sale                 |	 POST  |  http://localhost:8090/api/sales/
Delete a sale	              | DELETE |  http://localhost:8090/api/sales/:id

### Inventory API (Optional)
 - Put Inventory API documentation here. This is optional if you have time, otherwise prioritize the other services.

### Service API
    This is not your average service department. Here at CarCar, we hire the BEST Technicians and train them for quality
    guest services before they even touch a tool.
    We have integrated this system so that Technicians can be added, removed, or viewed.
    Technicians can be selected, via drop down menu, for service appointments, so you dont miss service from your favorite tech!
    If the vehicle going in for service was purchased here at CarCar, we have that vehicle's appointment dubbed as VIP! That means
    FREE oil changes, car washes, and tire rotations for as long as you own the vehicle! Amazing, right?
    Ahead in reading, you will find process documentation on how to interact with this wonderful API, and dont worry, it's not
    as daunting as it sounds.

In order to CREATE a Technician you'll need three things:

    {
        "first_name": "Example",
        "last_name": "Example",
        "employee_id": "Example"
    }

    It's really that easy!

    Now, on the backend there is a technician_id, that gets automatically fixed to any created technician; However, I have implemented
    it so that you will only see their "first_name last_name" when adding a tech to an appointment via React interaction. Why remember
    ID AND an employee_id? Let's make your life EASIER!
    That being said, if you need to delete a technician, or create an appointment tied to a tech, I have left the ID in the "list Techs"
    via insomnia so that when you need that information, it is easily accessible!

To DELETE a tech:
    list techs in insomnia and locate the technician you would like to remove.
    look at their "id" key value, and remember it (or write it down, if you're like me)
    click on delete tech and after the "technicians/" portion of the URL insert that id number followed by a "/"
        eaxmple: ' http://localhost:8080/api/technicains/#/"
    once you delete you should see "deleted:true"
    now, by the off chance you see "deleted:false" either that technician has already been deleted, or simply does not exist. Double
    check the ID number and try again.
    For clarification; the property you need is ID and NOT eployee_id.

In order to CREATE an appointment, you'll need:

    {
        "vin": "Example",
        "customer": "Example",
        "date_time":"MM/DD/YYYY ##:##",
        "service_reason":"Example",
        "technician":"Example"
    }

    If the VIN matches any vehicles that were in our Automobile inventory, it will automatically determine that the guest is a VIP member,
    and will be valued on the React page showing appointments. If it says "No," under "VIP?," still be nice, just be sure they get their invoice
    for the VIP-freebies!

    Now, there is a field entitled "STATUS" that will only be set as "cancelled" or "finished" once the button is clicked to determine which,
    that feature is located in the Services Appointments listing via React. Any time a vehicle in service appointments is given a status assignment,
    it will disappear from pending service appointments, but don't fret! You can find a complete history of vehicle service appointments via the Service
    History nav link! AND whats even better... you can search BY VIN in either place! Just type in the VIN number and as you do, the VINs not
    containing those letters with dissapear. Like Magic! So, when a guest comes in and their paperwork has smudges, coffee stains, or my personal
    favorite... dog chewings, and you can only see five letters of the VIN; you should still be able to find it. Neat, huh?

    As far as deleting appointments go, its the same as deleting a technician, find the ID listed in insomnia via "list apps" and type that into
    the "delete apps" URL after "appointments/#/" dont forget the "/" afterward, Django really likes those things.

    Well, I hope that you have enjoyed learning about our Services Section and hope to hear that it was relatively easy to navigate! Have fun!

### Sales API
 ## Sales microservice

Explain your models and integration with the inventory
microservice, here.

The Sales Services Microservice tracks car sales, recording details about the salesperson, customer, and the sold automobile. The Inventory Microservice acts as a catalog, listing car manufacturers, models, and specific automobiles available at a dealership.These two services work together to ensure that cars are accurately listed in the inventory and properly recorded when sold. Together, these services create a complete system that not only showcases available cars but also efficiently manages and records of the car sales process, ensuring a transparent and reliable sales history.

### Customers:

- Manage customer information with the following endpoints:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List customers | GET | http://localhost:8090/api/customers/
| Create a customer | POST | http://localhost:8090/api/customers/
| Show a specific customer | GET | http://localhost:8090/api/customers/id/

To create a Customer (SEND THIS JSON BODY) Method: POST
```
{
    "first_name": "Henry",
    "last_name": "Smith",
    "address": "123 Main St, Queens, NY",
    "phone_number": "125-456-8481"
}
```
Return Value of Creating a Customer Method: GET
```
{
	"first_name": "Henry",
	"last_name": "Smith",
	"address": "123 Main St, Queens, NY",
	"phone_number": "125-456-8481"
}
```
Return value of Listing all Customers Method: GET
```
{
	"customers": [
		{
			"first_name": "Henry",
			"last_name": "Smith",
			"address": "123 Main St, Queens, NY",
			"phone_number": "125-456-8481"
		},
		{
			"first_name": "May",
			"last_name": "Smith",
			"address": "33 Main St, Queens, NY",
			"phone_number": "225-456-8481"
		}
	]
}
```
### Salespeople:

- Manage salespeople with the following endpoints:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople | GET | http://localhost:8090/api/salespeople/
| Salesperson details | GET | http://localhost:8090/api/salesperson/id/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| Delete a salesperson | DELETE | http://localhost:8090/api/salesperson/id/


To create a salesperson (SEND THIS JSON BODY) Method: POST
```
{
	"first_name": "Jack",
	"last_name": "Lee",
	"employee_id": 14
}
```
Return Value of creating a salesperson Method: GET
```
{
	"first_name": "Jack",
	"last_name": "Lee",
	"employee_id": 123,
	"id": 1
}
```
List all salespeople Return Value Method: GET
```
{
	"salespeople": [
		{
			"first_name": "Jack",
			"last_name": "Lee",
			"employee_id": 123,
			"id": 1
		},
		{
			"first_name": "Yutong",
			"last_name": "Ye",
			"employee_id": 35,
			"id": 2
		},
    ]
}
```
### Sales History:

- Manage sales records with the following endpoints:
- the id value to show a salesperson's salesrecord is the id tied to a saleperson

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List all salesrecords | GET | http://localhost:8090/api/sales/
| Create a new sale | POST | http://localhost:8090/api/sales/
| Show salesperson's salesrecords | GET | http://localhost:8090/api/sales/id/
List all Salesrecords Return Value:
```
Create a New Sale (SEND THIS JSON BODY) Method: POST
```
{
    "automobile": "1C3CC5FB2AN120174",
    "customer": "1",
    "price": 111000,
	"salesperson": 1
}
```
Return Value of creating a New Sale Method: GET
```
{
	"sales": [
		{
			"id": 1,
			"price": 111000,
			"vin": {
				"vin": "111"
			},
			"salesperson": {
				"id": 1,
				"name": "Liz",
				"employee_number": 1
			},
			"customer": {
				"name": "Martha Stewart",
				"address": "1313 Baker Street",
				"phone_number": "980720890"
			}
		}
	]
}
```
List all sale Return Value Method: GET
```
{
	"sales": [
		{
			"id": 1,
			"price": 111000,
			"vin": {
				"vin": "111"
			},
			"salesperson": {
				"id": 1,
				"name": "Liz",
				"employee_number": 1
			},
			"customer": {
				"name": "Martha Stewart",
				"address": "1313 Baker Street",
				"phone_number": "980720890"
			}
		}
	    {
            "id": 4,
	        "price": 40000,
	        "vin": {
		        "vin": "888"
	        },
	        "salesperson": {
		        "id": 1,
		        "name": "Liz",
		        "employee_number": 1
	        },
	        "customer": {
		        "id",
		        "name": "John Johns",
		        "address": "1212 Ocean Street",
		        "phone_number": "9804357878"
            }
	    }
    ]
}
