import React, { useEffect, useState } from 'react';


function ManForm() {
    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [manufacturers, setMans] = useState('');
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setMans(data.manufacturers)
        }
    }

    const handleSubmit = async (event) => {
        const data = {
            name,
            };

        const manUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(manUrl, fetchConfig);
        if (response.ok) {
            const newMan = await response.json();
            console.log(newMan);
            setName('');
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create A New Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-man-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="manufacturer" value={manufacturers.name} id="manufacturer" className="form-control" />
                            <label htmlFor="manufacturer">Manufacturer Name</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        );
    }

    export default ManForm;
