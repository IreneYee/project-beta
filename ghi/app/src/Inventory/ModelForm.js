import React, { useState, useEffect } from 'react';

function ModelForm() {
  const [newModel, setNewModel] = useState({ 
    name: '', 
    picture_url: '', 
    manufacturer_id: '' });
  const [manufacturers, setManufacturers] = useState([]);

  useEffect(() => {

    const fetchManufacturers = async () => {
      const response = await fetch('http://localhost:8100/api/manufacturers/');
      if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers);
      }
    };

    fetchManufacturers();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    await fetch('http://localhost:8100/api/models/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newModel),
    });

    setNewModel({ 
      name: '', 
      picture_url: '', 
      manufacturer_id: '' });
  };

  const handleChange = (event) => {
    setNewModel({ 
      ...newModel, 
      [event.target.name]: event.target.value 
    });
  };



  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a Vehicle Model</h1>
            <form onSubmit={handleSubmit}>

                <div className="form-floating mb-3">
                     <input  onChange={handleChange} required placeholder="Name" value={newModel.name} type='text' name='name' id='name'className='form-control'/>
                     <label htmlFor="name">Model name...</label>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handleChange} required placeholder="url" value={newModel.picture_url} type='url' name='picture_url' id='picture_url' className='form-control'  />
                    <label htmlFor="picture_url">Picture URL...</label>
                </div>

                <div className="form-floating mb-3">
                    <select id='manufacturer_id' name='manufacturer_id' className='form-control' value={newModel.manufacturer_id} onChange={handleChange} required>
                    <option value=''>Choose Manufacturer...</option>
                   {manufacturers.map((manufacturer) => (
                   <option key={manufacturer.id} value={manufacturer.id}> 
                   {manufacturer.name}
                   </option>
            ))}
          </select>
        </div>
        <button type='submit' className='btn btn-primary'>Submit</button>
      </form>
    </div>
    </div>
    </div>
  );
}

export default ModelForm;


