import { useEffect, useState } from 'react';

function ManufacturersList() {
    const [manufacturers, setMans] = useState([]);

    const getData = async() => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');

        if (response.ok) {
            const data = await response.json();
            setMans(data.manufacturers)
        }
    }

    useEffect(()=>{
        getData()
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Manufacturers List</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer Name</th>
                </tr>
            </thead>
            <tbody>
                {manufacturers.map(manufacturers => {
                    const key = manufacturers.id;
                    return (
                    <tr key={key}>
                        <td>{manufacturers.name}</td>
                    </tr>
                    );
                    })}
            </tbody>
        </table>
        </div>
        </div>
        </div>
    );
}

export default ManufacturersList;
