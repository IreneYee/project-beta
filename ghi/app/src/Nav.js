import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <li className="container-fluid">
              <NavLink className="navbar-brand" to="/models">Models</NavLink>
        </li>
        <li className="container-fluid">
              <NavLink className="navbar-brand" to="/create_model">Create a Model</NavLink>
        </li>
        <li className="container-fluid">
              <NavLink className="navbar-brand" to="/salespeople">Salespeople</NavLink>
        </li>
        <li className="container-fluid">
              <NavLink className="navbar-brand" to="/create_salesperson">Add a Salesperson</NavLink>
        </li>
        <li className="container-fluid">
              <NavLink className="navbar-brand" to="/customer">Customers</NavLink>
        </li>
        <li className="container-fluid">
              <NavLink className="navbar-brand" to="/create_customer">Add a Customers</NavLink>
        </li>
        <li className="container-fluid">
              <NavLink className="navbar-brand" to="/Saleslist">Sales</NavLink>
        </li>
        <li className="container-fluid">
              <NavLink className="navbar-brand" to="/Salesform">Add a Sale</NavLink>
        </li>
        <li className="container-fluid">
              <NavLink className="navbar-brand" to="/Salesperson_History">Salesperson History</NavLink>
        </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/">List Manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/new/">Create A Manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/">List Automobiles</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/new/">Create An Automobile</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/technicians/">List Technicians</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/technicians/new/">Create A Technician</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/appointments/">List Appointments</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/appointments/new/">Create An Appointment</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/appointment/history/">Service History</NavLink>
            </li>
      </div>
    </nav>
  )
}

export default Nav;
