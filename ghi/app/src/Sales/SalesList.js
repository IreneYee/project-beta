import React, { useState, useEffect } from 'react';

function SalesList() {
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [prices, setPrices] = useState([]);

    const fetchSalespeople = async () => {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);
        
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople); 
        }
    };

    const fetchCustomers = async () => {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch(url);
        
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers); 
        }
    };

    const fetchPrices = async () => {
        const url = "http://localhost:8090/api/sales/";
        const response = await fetch(url);
        
        if (response.ok) {
            const data = await response.json();
            setPrices(data.prices); 
        }
    };

    useEffect(() => {
        fetchSalespeople();
        fetchCustomers();
        fetchPrices();
    }, []);

    return (
        <div>
            <h1>Sales</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map((salesperson, index) => (
                        <tr key={salesperson.employee_id}>
                            <td>{salesperson.employee_id}</td>
                            <td>{salesperson.first_name + " " + salesperson.last_name}</td>
                            <td>{index < customers.length ? customers[index].first_name + " " + customers[index].last_name : "None"}</td>
                            <td>{"4Y1SL65848Z411439"}</td> 
                            <td>{"20,000"}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default SalesList;