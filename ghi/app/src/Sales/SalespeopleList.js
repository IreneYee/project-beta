import React, { useState, useEffect } from 'react'; 


function SalespeopleList({}) {
    const [salespeople, setSalespeople] = useState([]);

    const fetchData = async () => {
        const Url = "http://localhost:8090/api/salespeople/";

        const Response = await fetch(Url);
        
        if (Response.ok) {
            const data = await Response.json();
            setSalespeople(data.salespeople); 
        }
    };

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div>
            <h1>Salespeople</h1> 
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map(salesperson => (
                        <tr key={salesperson.employee_id}>
                            <td>{salesperson.employee_id}</td>
                            <td>{salesperson.first_name}</td>
                            <td>{salesperson.last_name}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default SalespeopleList;